// Products, categories, reviews
product = db.products.findOne({ 'slug': 'wheel-barrow-9092' })
o/p-
{
    "_id" : ObjectId("4c4b1476238d3b4dd5003981"),
    "slug" : "wheel-barrow-9092",
    "sku" : "9092",
    "name" : "Extra Large Wheel Barrow",
    "description" : "Heavy duty wheel barrow...",
    "details" : {
            "weight" : 47,
            "weight_units" : "lbs",
            "model_num" : NumberLong("4039283402"),
            "manufacturer" : "Acme",
            "color" : "Green"
    },
    "total_reviews" : 4,
    "average_review" : 4.5,
    "pricing" : {
            "retail" : 5897,
            "sale" : 4897
    },
    "price_history" : [
            {
                    "retail" : 5297,
                    "sale" : 4297,
                    "start" : ISODate("2010-05-01T00:00:00Z"),
                    "end" : ISODate("2010-05-08T00:00:00Z")
            },
            {
                    "retail" : 5297,
                    "sale" : 5297,
                    "start" : ISODate("2010-05-09T00:00:00Z"),
                    "end" : ISODate("2010-05-16T00:00:00Z")
            }
    ],
    "category_ids" : [
            ObjectId("6a5b1476238d3b4dd5000048"),
            ObjectId("6a5b1476238d3b4dd5000049")
    ],
    "main_cat_id" : ObjectId("6a5b1476238d3b4dd5000048"),
    "tags" : [
            "tools",
            "gardening",
            "soil"
    ]
}

// Find reviews of a specific product
reviews_count = db.reviews.count({ 'product_id': product['_id'] })
o/p-3

// summary of reviews for  all products
reviewCount = db.reviews.aggregate([
    {
        $group: {
            _id: '$product_id',
            count: { $sum: 1 }
        }
    }
]).next();
o/p-
{ "_id" : ObjectId("4c4b1476238d3b4dd5003981"), "count" : 3 }

// look up product first
product = db.products.findOne({ 'slug': 'wheel-barrow-9092' })
o/p-
{
    "_id" : ObjectId("4c4b1476238d3b4dd5003981"),
    "slug" : "wheel-barrow-9092",
    "sku" : "9092",
    "name" : "Extra Large Wheel Barrow",
    "description" : "Heavy duty wheel barrow...",
    "details" : {
            "weight" : 47,
            "weight_units" : "lbs",
            "model_num" : NumberLong("4039283402"),
            "manufacturer" : "Acme",
            "color" : "Green"
    },
    "total_reviews" : 4,
    "average_review" : 4.5,
    "pricing" : {
            "retail" : 5897,
            "sale" : 4897
    },
    "price_history" : [
            {
                    "retail" : 5297,
                    "sale" : 4297,
                    "start" : ISODate("2010-05-01T00:00:00Z"),
                    "end" : ISODate("2010-05-08T00:00:00Z")
            },
            {
                    "retail" : 5297,
                    "sale" : 5297,
                    "start" : ISODate("2010-05-09T00:00:00Z"),
                    "end" : ISODate("2010-05-16T00:00:00Z")
            }
    ],
    "category_ids" : [
            ObjectId("6a5b1476238d3b4dd5000048"),
            ObjectId("6a5b1476238d3b4dd5000049")
    ],
    "main_cat_id" : ObjectId("6a5b1476238d3b4dd5000048"),
    "tags" : [
            "tools",
            "gardening",
            "soil"
    ]
}

// rating summary - for selected product
totalRating = db.reviews.aggregate([
    { $match: { product_id: product['_id'] } },
    {
        $group: {
            _id: '$product_id',
            count: { $sum: 1 }
        }
    }
]).next();
o/p-{ "_id" : ObjectId("4c4b1476238d3b4dd5003981"), "count" : 3 }


// Adding average review 
o/p-

{
    "_id" : ObjectId("4c4b1476238d3b4dd5003981"),
    "average" : 4.333333333333333,
    "count" : 3
}

// below verifies that in fact, the first command will use an index, second will not
db.reviews.ensureIndex({ product_id: 1 })

countsByRating = db.reviews.aggregate([
    { $match: { 'product_id': product['_id'] } },
    {
        $group: {
            _id: '$rating',
            count: { $sum: 1 }
        }
    }
], { explain: true })

countsByRating = db.reviews.aggregate([
    {
        $group: {
            _id: { 'product_id': '$product_id', rating: '$rating' },
            count: { $sum: 1 }
        }
    },
    { $match: { '_id.product_id': product['_id'] } }
], { explain: true })


ratingSummary = db.reviews.aggregate([
    {
        $group: {
            _id: '$product_id',
            average: { $avg: '$rating' },
            count: { $sum: 1 }
        }
    },
    { $match: { '_id': product['_id'] } }
]).next();

o/p-

// Counting Reviews by Rating
countsByRating = db.reviews.aggregate([
        { $match: { 'product_id': product['_id'] } },
        {
            $group: {
                _id: '$rating',
                count: { $sum: 1 }
            }
        }
    ]).toArray();

    o/p-
    
    // Joining collections
    db.products.aggregate([
        {
            $group: {
                _id: '$main_cat_id',
                count: { $sum: 1 }
            }
        }
    ]);
    
    o/p-
    
    
    // findOne on mainCategorySummary
    
    db.mainCategorySummary.findOne()
    
    // Faster Joins - $unwind
    
    // FASTER JOIN - $UNWIND
    db.products.aggregate([
        { $project: { category_ids: 1 } },
        { $unwind: '$category_ids' },
        {
            $group: {
                _id: '$category_ids',
                count: { $sum: 1 }
            }
        },
        { $out: 'countsByCategory' }
    ]);

    o/p-
    
    